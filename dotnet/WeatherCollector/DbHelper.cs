﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherCollector.Db;

namespace WeatherCollector
{
	public class DbHelper
	{
		public static Setting CurrentSettings
		{
			get
			{
				Setting result;

				using (WeatherHistoryEntities db = new WeatherHistoryEntities())
				{
					result = db.Settings.ToList().LastOrDefault();
				}
				if (result == null)
				{
					ThirdParty.Logger.Error("Settings from DB cannot be null");
					//todo: throw exception and handle it from outside
				}

				return result;
			}
		}

		public enum WeatherProvider
		{
			OpenMapWeatherProvider = 1,
			UndergroundWeatherProvider = 2
		}
	}
}
