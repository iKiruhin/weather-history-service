﻿using System.ServiceProcess;
using AutoMapper;
using CreativeGurus.Weather.Wunderground.Models;
using OpenWeatherMap;
using WeatherCollector.Model;

namespace WeatherCollector
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ConfigureAutoMapper();

			ServiceBase[] ServicesToRun;
			ServicesToRun = new ServiceBase[]
			{
				new WeatherCollector.WeatherCollectorService()
			};
			ServiceBase.Run(ServicesToRun);
		}

		private static void ConfigureAutoMapper()
		{
			Mapper.Initialize(cfg =>
			{
				cfg.CreateMap<WeatherItem, WeatherDto>() // OpenWeatherMap provider data
					.ForMember(dest => dest.Temperature, opt => opt.MapFrom(src => src.Temperature.Value))
					.ForMember(dest => dest.Pressure, opt => opt.MapFrom(src => src.Pressure.Value))
					.ForMember(dest => dest.Humidity, opt => opt.MapFrom(src => src.Humidity.Value))
					.ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Weather.Value))
					;
				cfg.CreateMap<SimpleForecastDay, WeatherDto>() // Wunderground provider data
					.ForMember(dest => dest.Temperature, opt => opt.MapFrom(src => src.High.Celsius))
					.ForMember(dest => dest.Pressure, opt => opt.MapFrom(src => src.Pop.Value))
					.ForMember(dest => dest.Humidity, opt => opt.MapFrom(src => (int)src.AveHumidity))
					.ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Conditions))
					;
			});
		}
	}
}