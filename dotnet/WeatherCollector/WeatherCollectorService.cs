﻿using System;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using NLog;
using OpenWeatherMap;
using WeatherCollector.Db;
using WeatherCollector.Model;
using Timer = System.Timers.Timer;

namespace WeatherCollector
{
	public partial class WeatherCollectorService : ServiceBase
	{
		Thread processThread;
		Timer timer;
		bool cancelFlag;

		protected override void OnStart(string[] args)
		{
			double checkIntervalInSeconds = DbHelper.CurrentSettings.CheckInterval;

			timer = new Timer(1000 * checkIntervalInSeconds); // run Timer_Tick() once in an hour
			timer.Elapsed += Timer_Tick;
			timer.Start();

			cancelFlag = false;

			ThirdParty.Logger.Info("===== Windows service started =====");

			DoWork(); // first time run
		}

		protected override void OnContinue()
		{
			timer.Start();

			ThirdParty.Logger.Info("OnContinue");
		}

		protected override void OnPause()
		{
			timer.Stop();

			ThirdParty.Logger.Info("OnPause");
		}

		protected override void OnStop()
		{
			ThirdParty.Logger.Info("Windows service is stopping");

			timer.Stop();

			if (processThread?.ThreadState != ThreadState.Running)
				return;

			cancelFlag = true;
			// Give thread a chance to stop
			processThread.Join(500);
			processThread.Abort();
		}

		void Timer_Tick(object sender, EventArgs e)
		{
			processThread = new Thread(DoWork);
			processThread.Start();

			ThirdParty.Logger.Info("Timer_Tick");
		}

		private void DoWork()
		{
			ThirdParty.Logger.Info("DoWork");

			WeatherChecker wc = new WeatherChecker();
			try
			{
				if (cancelFlag)
					return;

				wc.GetAndSaveCurrentWeather();
			}
			catch (Exception ex)
			{
				ThirdParty.Logger.Error(ex,
					"Exception during data getting/saving . " + ex.Message + ". Inner Exception: " + ex.InnerException?.Message);
			}
			finally
			{
				wc.Dispose();
			}
		}
	}
}
