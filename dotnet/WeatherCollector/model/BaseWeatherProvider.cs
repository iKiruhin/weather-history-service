﻿using System.Threading.Tasks;
using OpenWeatherMap;

namespace WeatherCollector.Model
{
	public abstract class BaseWeatherProvider
	{
		public abstract Task<WeatherDto> GetCurrentWeather(string englishCityName);
		protected abstract string apiKey { get; }
	}
}
