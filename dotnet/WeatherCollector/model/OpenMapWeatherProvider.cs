﻿using System.Threading.Tasks;
using AutoMapper;
using OpenWeatherMap;

namespace WeatherCollector.Model
{
	public class OpenMapWeatherProvider : BaseWeatherProvider
	{
		protected override string apiKey => "035afa662ed88d0f7ea87e787e04fe31";

		public override async Task<WeatherDto> GetCurrentWeather(string englishCityName)
		{
			var client = new OpenWeatherMapClient(apiKey);
			var currentWeather = await client.CurrentWeather.GetByName(englishCityName);
			ThirdParty.Logger.Info(currentWeather.Weather.Value);

			return Mapper.Map<WeatherItem, WeatherDto>(currentWeather);
		}
	}
}