﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CreativeGurus.Weather.Wunderground;
using CreativeGurus.Weather.Wunderground.Models;
using OpenWeatherMap;

namespace WeatherCollector.Model
{
	public class UndergroundWeatherProvider : BaseWeatherProvider
	{
		protected override string apiKey => "664025a77ffe647b";
		
		public override async Task<WeatherDto> GetCurrentWeather(string englishCityName)
		{
			WeatherClient client = new WeatherClient(apiKey);
			var result = await client.GetForecastAsync(QueryType.GlobalCity, new QueryOptions() { City = englishCityName }); //Gets data by city

			return Mapper.Map<SimpleForecastDay, WeatherDto>(result.Forecast.SimpleForecast.ForecastDay[0]);
		}
	}
}