﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenWeatherMap;
using WeatherCollector.Db;

namespace WeatherCollector.Model
{
	public class WeatherChecker : IDisposable
	{
		WeatherHistoryEntities db;

		public WeatherChecker()
		{
			db = new WeatherHistoryEntities();
		}
		public void Dispose()
		{
			db.Dispose();
		}

		public void GetAndSaveCurrentWeather()
		{
			var cw = GetCurrentWeather();
			SaveWeather(cw);
		}

		private Task<WeatherDto> GetCurrentWeather()
		{
			ThirdParty.Logger.Info("GetCurrentWeather");

			BaseWeatherProvider weatherService = null;
			DbHelper.WeatherProvider weatherProvider = (DbHelper.WeatherProvider)DbHelper.CurrentSettings.CurrentProviderId;

			switch (weatherProvider)
			{
				case DbHelper.WeatherProvider.OpenMapWeatherProvider:
					weatherService = new OpenMapWeatherProvider();
					break;
				case DbHelper.WeatherProvider.UndergroundWeatherProvider:
					weatherService = new UndergroundWeatherProvider();
					break;
				default:
					ThirdParty.Logger.Error("Unknown weather provider.");
					//todo: throw exception (and handle outside)
					break;
			}

			string engCityName = db.Cities.Find(DbHelper.CurrentSettings.CurrentCityId)?.EnglishName;
			return weatherService.GetCurrentWeather(engCityName);
		}

		private void SaveWeather(Task<WeatherDto> cw)
		{
			ThirdParty.Logger.Info("SaveWeather");

			WeatherRequest wRequest = new WeatherRequest
			{
				CityId = DbHelper.CurrentSettings.CurrentCityId,
				ProviderId = DbHelper.CurrentSettings.CurrentProviderId,
				Details = cw.Result.ToString(),
				RequestDate = DateTime.Now
			};
			db.WeatherRequests.Add(wRequest);
			db.SaveChanges();
		}

	}
}
