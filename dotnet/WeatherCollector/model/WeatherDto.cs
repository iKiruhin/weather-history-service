﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherCollector.Model
{
	public class WeatherDto
	{
		public double? Temperature { get; set; }
		public int Pressure { get; set; }
		public int? Humidity { get; set; }
		public string Description { get; set; }

		public override string ToString()
		{
			return $"It's {Description}. Temperature (possibly in Kelvins): {Temperature}; Humidity: {Humidity}; Pressure: {Pressure}";
		}
	}
}
