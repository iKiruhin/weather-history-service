<?php

class DbHelper
{
    private $serverName;
    private $userName;
    private $password;
    private $dbName;

    function __construct()
    {
        $this->serverName = "localhost";
        $this->username = "sa";
        $this->password = "dbadmin!1";
        $this->dbName = "WeatherHistory";
    }

    public static function OutputDataTableAsSelect($tableName, $columnName)
    {
        $result = "<select name='$tableName' id='$tableName'>";
        $db = new DbHelper();

        try {
            $tsql = "SELECT ID, " . $columnName . " FROM " . $tableName;
            $items = $db->Execute($tsql);
            foreach ($items as $row) {
                $result .= "<option value=$row[ID]>$row[$columnName]</option>";
            }
        } catch (Exception $e) {
            die(print_r($e->getMessage()));
        }
        $result .= "</select>";

        return $result;
    }

    public static function SaveSettings($providerId, $cityId, $checkInterval)
    {
        $result = "saving settings failed";
        $db = new DbHelper();

        try {
            $tsql = "INSERT INTO [Settings]
                           ([CurrentCityId]
                           ,[CurrentProviderId]
                           ,[CheckInterval])  
                      VALUES (?,?,?)";
            $params = array(&$cityId, &$providerId, &$checkInterval);

            $db->Execute($tsql, $params, True);
            $result = "settings successfully saved to DB.";
        } catch (Exception $e) {
            die(print_r($e->getMessage()));
        }

        return $result;
    }

    public function Execute($tsql, $params = NULL, $isNonQuery = NULL)
    {
        try {
            $connection = new PDO("sqlsrv:Server=$this->serverName;Database=$this->dbName", $this->username, $this->password);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            die(print_r($e->getMessage()));
        }

        $getItems = $connection->prepare($tsql);
        $getItems->execute($params);
        if (!$isNonQuery) {
            $items = $getItems->fetchAll(PDO::FETCH_ASSOC);
            return $items;
        }
    }

    public function __get($name)
    {
        return isset($this->$name) ? $this->$name : null;
    }

    public
    function getServerName()
    {
        return $this->serverName;
    }

    public
    function getUserName()
    {
        return $this->userName;
    }

    public
    function getPassword()
    {
        return $this->password;
    }

    public
    function getDbName()
    {
        return $this->dbName;
    }


}