<?php

class RenderHelper
{
    static function abc($param1 , $param2)
    {
        return "$param1 , $param2";
    }

    static function BeginTableRender($headings)
    {
        echo "<table align='center' cellpadding='5'>";
        foreach ($headings as $heading) {
            echo "<th>$heading</th>";
        }
        echo "</tr>";
    }

    static function RenderTableRow($values)
    {
        echo "<tr>";
        foreach ($values as $val) {
            echo    "<td>$val</td>";
        }
        echo "</tr>";
    }

    static function EndTableRender()
    {
        echo "</table><br/>";
    }

    static function DisplayNoItemsMessage($message)
    {
        echo "<h4 align='center'>$message</h4>";
    }
}