<html>
<head>
    <title>Weather requests settings.</title>
</head>
<body>
<?php
include 'controllers/DbHelper.php';
include 'controllers/RenderHelper.php';

CheckAndHandlePostback();
OutputCurrentSettings();
OutputForm();

function CheckAndHandlePostback(){
    if (!isset($_POST['saveBtn']))
        return;

    $providerId = $_POST['Provider'];
    $cityId = $_POST['City'];
    $checkInterval = $_POST['checkInterval'];

    echo DbHelper::SaveSettings($providerId, $cityId, $checkInterval);
}
function OutputForm(){
    $self = $_SERVER['PHP_SELF'];
    echo "
<form action='$self' method='post' name='form'>
    <table align='center' cellpadding='15'>
        <tr>  
            <td align='center'>
                Change provider:</br>";
                echo DbHelper::OutputDataTableAsSelect('Provider', 'Name');
            echo "</td>
            <td align='center'>  
                Change city:</br>";
                echo DbHelper::OutputDataTableAsSelect('City', 'EnglishName');
            echo "</td>
            <td align='center'>  
                Change check interval in seconds:</br>
                <input type='number' name='checkInterval' value='3600' />
           </td>  
            <td align='center'>  
                Change provider:</br>
                <input type='submit' name='saveBtn' value='Save'/>
            </td>  
        </tr>  
    </table> 
</form>";
}

function OutputCurrentSettings()
{
    $db = new DbHelper();

    try {
        $tsql = "SELECT TOP 1 
                  [Id]
                  ,[ProviderName]
                  ,[ProviderUrl]
                  ,[CityRussianName]
                  ,[CityEnglishName]
                  ,[CheckInterval]
                  ,[ChangeDate]
              FROM [View_AllSettings]
              ORDER BY Id DESC";

        $items = $db->Execute($tsql);
        $itemCount = count($items);
        if ($itemCount > 0) {
            $headings = array("Current provider", "Current city", "Check interval", "Change date");
            RenderHelper::BeginTableRender($headings);
            foreach ($items as $row) {
                $columns = array(
                    "<a href=$row[ProviderUrl] target=\'_blank\'>$row[ProviderName]</a>",
                    $row['CityEnglishName'],
                    $row['CheckInterval'],
                    $row['ChangeDate']
                );
                RenderHelper::RenderTableRow($columns);
            }
            RenderHelper::EndTableRender();
        } else {
            RenderHelper::DisplayNoItemsMessage("No settings saved in DB. Add settings to apt DB table or address to your system administrator");
        }
    } catch (Exception $e) {
        die(print_r($e->getMessage()));
    }
}

?>
</body>
</html>