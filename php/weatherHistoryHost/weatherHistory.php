<html>
<head>
    <title>Weather requests history.</title>
</head>
<body>
<?php
include 'controllers/DbHelper.php';
include 'controllers/RenderHelper.php';


$db = new DbHelper();

/* select from db table */
try {
    $tsql = "SELECT Id
                  ,[ProviderName]
                  ,[ProviderUrl]
                  ,[CityEnglishName]
                  ,[CityRussianName]
                  ,[WeatherDetails]
                  ,[RequestDate]
              FROM [View_WeatherRequests]
              ORDER BY Id DESC";

    $items = $db->Execute($tsql);
    $itemCount = count($items);
    if ($itemCount > 0) {
        $headings = array("Provider", "City", "Details", "Time");
        RenderHelper::BeginTableRender($headings);
        foreach ($items as $row) {
            $columns = array(
                "<a href=$row[ProviderUrl] target=\'_blank\'>$row[ProviderName]</a>",
                $row['CityEnglishName'],
                $row['WeatherDetails'],
                $row['RequestDate']
            );
            RenderHelper::RenderTableRow($columns);
        }
        RenderHelper::EndTableRender();
    } else {
        RenderHelper::DisplayNoItemsMessage("No request history yet. May be you have to wait a bit... or check settings page");
    }
} catch (Exception $e) {
    die(print_r($e->getMessage()));
}
?>
</body>
</html>